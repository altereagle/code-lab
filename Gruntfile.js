/*
  Converts Jade, CoffeeScript and Stylus files 
  to .html, .js and .css files.
 */

module.exports = function(grunt) {
  var settings     = require("./settings");
  var rootFolder   = settings.tasks.rootFolder;
  var inputFolder  = rootFolder + settings.tasks.inputFolder;
  var outputFolder = rootFolder + settings.tasks.outputFolder;

  var obj;
  grunt.initConfig({
    pkg: '<json:package.json>',
    watch: {
      "default": {
        options: {
          spawn: false,
          interrupt: true
        },
        files: [inputFolder + "/**/*.jade", inputFolder + "/**/*.coffee", inputFolder + "/**/*.styl"],
        tasks: ['stylus', 'coffee', 'jade', 'docco']
      }
    },
    jade: {
      compile: {
        options: {
          client: false,
          pretty: true,
          data: {
            debug: true
          }
        },
        files: [
          {
            cwd: "" + inputFolder,
            src: '**/*.jade',
            dest: "" + outputFolder,
            ext: '.html',
            expand: true
          }, {
            cwd: rootFolder + "/",
            src: '*.jade',
            dest: outputFolder + "/",
            ext: '.html',
            expand: true
          }
        ]
      }
    },
    coffee: {
      compile: {
        expand: true,
        cwd: "" + inputFolder,
        src: ['**/*.coffee'],
        dest: "" + outputFolder,
        ext: '.js',
        options: {
          bare: true,
          preserve_dirs: true
        }
      }
    },
    stylus: {
      compile: {
        expand: true,
        cwd: "" + inputFolder,
        src: ['**/*.styl'],
        dest: "" + outputFolder,
        ext: '.css'
      }
    },
    docco: {
      dependencies: {
        src: ["test/dependencies/*.coffee"],
        options: {
          output: 'docs/server/dependencies'
        }
      },
      settings: {
        src: ["test/settings/*.coffee"],
        options: {
          output: 'docs/server/settings'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-docco');
  return grunt.registerTask('default', ['stylus', 'coffee', 'jade', 'docco','watch']);
};