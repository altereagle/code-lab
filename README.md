# Code Lab
==========

# Authors
=========
- Kendell Joseph
- Cole Nemec
- Thomas Wissemann
- Jake Harper
- Brant Edmiston
- TJ George
- Henry Dewey
- J. Holland
- Nicholas Strehli

# Arduino Resources
====================
 Online Ardunio IDE: codebender.cc

# TODO
======
Monday - 11/30/2015
1. Create a folder for your game in codelab/projects/individual/YOUR-NAME-HERE
2. Create these initial game files
    - index.html
    - script.js
    - style.css
3. Add your name to the authors list in this README