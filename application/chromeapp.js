/* global chrome */
// Create the first application window
chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('index.html', {
    id: "trinitygames",
    frame: {
      color: "#000"
    },
    innerBounds: {
      minWidth : 1000,
      minHeight: 800
    }
  });
});