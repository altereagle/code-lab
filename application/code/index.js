// Arcade game list
var games = [{
  name       : "Whymsica",
  description: "Insert description here"
}, {
  name       : "Maze",
  description: "Insert description here",
  video      : "videos/intro.mp4",
  themeSong  : "sounds/maze theme song.mp3"
}];

var initialGameIndex = 0;

// -----------------
var audio    = new Audio('sounds/maze theme song.mp3');
audio.volume = 0.15;

var audio2    = new Audio('sounds/big press.mp3');
audio2.volume = 0.15;

var video         = document.querySelector("game-video > video");
var logo          = document.querySelector("game-logo");
var title         = document.querySelector("game-title");
var gameSelection = document.querySelector("game-selection");
var body          = document.querySelector("body");

var gameState = {
  started: false
};

// Interaction Events
// -----------------
logo.addEventListener("mouseover", function(){
  audio.volume = 0.25;
  audio.play();
  video.play();

  video.style.opacity = 0.25;
  title.classList.add("underlined");
  title.classList.remove("active");
});

logo.addEventListener("mouseout", function(){
  // Pause the audio if the game has started
  if(!gameState.started) {
    audio.volume = 0.15;                  // Reset the audio volume
    audio.pause();                        // Pause the audio
    video.style.opacity   = 0;            // Hide the video
    body.style.background = "#000";       // Reset the background color
    title.classList.remove("underlined");
    title.classList.add("active");
  }
});

logo.addEventListener("click", function(){
  // Set the game state to started
  gameState.started = true;

  audio2.currentTime = 0;
  audio2.volume      = 1;
  audio2.play();

  audio.volume = 0.05;

  video.style.opacity   = 0;

  // Hide the logo
  logo.style.display    = "none";
  body.style.background = "#555";

  // Show the game selection screen
  gameSelection.style.display = "flex";
});


// Game Selector Mechancis
// -----------------------
var chooseGameButtons = document.querySelectorAll("choose-game-button");
for(var buttonIndex in chooseGameButtons){
  var button        = chooseGameButtons[buttonIndex];
  if (!button.classList){ continue }

  button.addEventListener("click", function(event){
    var nextGameIndex = event.currentTarget.classList.contains("next") ? 1 : -1;
    initialGameIndex += nextGameIndex

    if(initialGameIndex < 0) {
      initialGameIndex = games.length;
    }

    if(initialGameIndex > games.length) {
      initialGameIndex = 0;
    }
    console.log(initialGameIndex);
    var game        = games[initialGameIndex];
    var video       = game.video;
    var themeSong   = game.themeSong;
    var description = game.description;

  });
}


/*
for( var gameIndex in games){
  var game        = games[gameIndex];
  var video       = game.video;
  var themeSong   = game.themeSong;
  var description = game.description;

}
*/