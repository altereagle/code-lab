# Code Lab Dependencies
dependencies =
  # Utilities
  path      : require 'path'
  fs        : require 'fs-extra'
  process   : require 'child_process'
  walk      : require 'fs-walk'
  mkdirp    : require 'mkdirp'
  watch     : require 'watch'
  colors    : require 'colors'
  async     : require 'async'
  promise   : require 'bluebird'
  _         : require 'underscore'
  mustache  : require 'mustache'
  bower     : require 'bower'

  # Security
  logger      : require 'express-logger'
  winston     : require 'winston'
  
  # Database
  database:
    neo4j   : require 'neo4j'

  # Web services
  express     : require 'express'
  http        : require 'http'
  requestify  : require 'requestify'
  session     : require 'express-session'
  morgan      : require 'morgan'
  cookieParser: require 'cookie-parser'
  bodyParser  : require 'body-parser'
  errorHandler: require 'express-error-handler'
  jquery      : require 'jquery'
  jade        : require 'jade'
  stylus      : require 'stylus'
  renderer    : require 'stylus-renderer'
  coffeeScript: require 'coffee-script'
  nib         : require 'nib'
  docco       : require 'docco'
  cheerio     : require 'cheerio'

  # Websockets
  io  : require 'socket.io'
  bone: require 'bone.io'

module.exports = dependencies