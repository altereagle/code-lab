colors = require "colors"
winston = require 'winston'
winston.emitErrs = true
logger = new winston.Logger
  transports: [
      new winston.transports.File
        name: 'InfoLog'
        level: 'info'
        filename: "#{process.cwd()}/logs/info.log"
        handleExceptions: true
        json: true
        maxsize: 5242880
        maxFiles: 5
        colorize: false
    ,
      new winston.transports.File
        name: 'WarnLog'
        level: 'warn'
        filename: "#{process.cwd()}/logs/warn.log"
        handleExceptions: true
        json: true
        maxsize: 5242880
        maxFiles: 5
        colorize: false
  ]
  exitOnError: false


logger.on 'logging', (transport, level, message)->
  console.log "‹!›".bold.yellow, message.white if level is "warn"
  console.log "‹♦›".bold.green, message.grey if level is "info"
  console.log "‹♠›".bold.red, message.bold.red if level is "error"

module.exports = logger
module.exports.stream = write: (message, encoding) ->
  logger.warn message
  return