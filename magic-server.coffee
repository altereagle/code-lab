http = require('http')
finalhandler = require('finalhandler')
serveStatic = require('serve-static')
serve  = serveStatic('./arcade')
server = http.createServer((req, res) ->
  done = finalhandler(req, res)
  serve req, res, done
  return
)
server.listen process.env.PORT
console.log "Magic Server Online"