// Setup express, socket.io, and http server
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = require('socket.io').listen(server);

// Configure bone.io options
var bone = require('bone.io');
bone.set('io.options', {
    server: io
});

// Serves bone.io browser scripts
app.use(bone.static());
app.use("/", express.static("client/"));
app.use("/bower", express.static("bower_components/"));

// Listen up
server.listen(process.env.PORT);
console.log("Chatroom Server Started");