/*
  * SPARKLE ELEMENT STUFF BELOW THIS LINE *
  -----------------------------------------
*/

// This function creates a wand sparkle element
function createSparkle(){
  return $("<wand-sparkle/>").appendTo($("body"));
}

// This function sets the position of a sparkle element on the screen
function setSparklePosition(sparkle, position){
  var sparklePosition = {
    top : position.y,
    left: window.innerWidth - position.x,
    background: position.color
  };
  
  //POSITIONAL NOTIFICATION BEGINS HERE
  var w = window.innerWidth;
  var h = window.innerHeight;
  
  //Location indicator vars
  var locationOnScreen1;
  var locationOnScreen2;
  
  //Sets vars based on location of tracker
  if(position.x < w/2){
    locationOnScreen2 = "Right";
  }
  if(position.x > w/2){
    locationOnScreen2 = "Left";
  }
  if(position.y < h/2){
    locationOnScreen1 = "Top ";
  }
  if(position.y > h/2){
    locationOnScreen1 = "Bottom ";
  }
  //Displays location
   var locationOutput = [locationOnScreen1 + locationOnScreen2];
   document.getElementById("posIndi").innerHTML = locationOutput;
  //POSITIONAL NOTIFICATION ENDS HERE
  
  
  sparkle.html(position.color)
  sparkle.addClass("appear");
  sparkle.css(sparklePosition);
  return sparkle;
}



// This function hides the sparkle element after some time has passed
function setSparkleTimeout(sparkle, timeout){
  timeout = timeout || {appear:1, hide: 1, remove:1};
  
  setTimeout(function(){
    sparkle.removeClass("appear");
    setTimeout(function(){
      sparkle.fadeOut(function(){
          sparkle.remove();
      });
    }, timeout.hide);
  }, timeout.appear);
}

// This function adds a sparkle on the screen
var sparkle = createSparkle();
function runFunctionsToCreateASparkle(wandPosition){
  setSparklePosition(sparkle, wandPosition);
  //setSparkleTimeout(sparkle);
}

/*
  * TRACKING STUFF BELOW THIS LINE *
  ----------------------------------
*/

// Create and add a video element to connect to the tracker
var videoElement = document.createElement("video");
videoElement.setAttribute("id", "trackVideo");
videoElement.setAttribute("width", window.innerWidth);
videoElement.setAttribute("height", window.innerHeight);
videoElement.setAttribute("preload", true);
videoElement.setAttribute("autoplay", true);
videoElement.setAttribute("loop", true);
videoElement.setAttribute("muted", true);
document.body.appendChild(videoElement);

// Set the type of tracker to use
var tracker = new tracking.ColorTracker(['magenta', 'cyan']);

// Create a tracker that uses the video element and the tracker
tracking.track(videoElement, tracker, {
  camera: true
});

// Turn on the camera and listen for tracking events
tracker.on("track", function(event) {
  if (!event.data.length) {
    return;
  }
  
  // Add a sparkle for every item found
  for(var i in event.data){
    runFunctionsToCreateASparkle(event.data[i]);
  }
  
  console.log("tracker data:", event.data.length);
});


