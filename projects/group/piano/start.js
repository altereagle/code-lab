var colors  = require("colors");
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = require("socket.io").listen(server);
var bone    = require("bone.io");

bone.set('io.options', {
  server: io
});

app.use(bone.static());
app.use(express.static('client'));
server.listen(8080);

console.log("Server has started!".cyan);