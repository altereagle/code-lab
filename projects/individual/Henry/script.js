(function() {
  var balloonImage, camera, debug, gameData, light, moveBalloon1, movementTime, onMouseMove, onWindowResize, playerBalloon1, render, renderer, scene, settings, splash, stats;

  splash = document.querySelector("#splash");

  debug = document.querySelector("#debug");

  settings = {
    view: {
      width: window.innerWidth,
      height: window.innerHeight
    },
    camera: {
      fov: 75,
      aspect: window.innerWidth / window.innerHeight,
      near: 0.1,
      far: 1000
    }
  };

  gameData = {
    player: {
      0: {
        x: 0,
        y: 0,
        z: 0
      },
      1: {
        x: 0,
        y: 0,
        z: 0
      }
    }
  };

  scene = new THREE.Scene();

  stats = new Stats;

  document.body.appendChild(stats.domElement);

  camera = new THREE.PerspectiveCamera(settings.camera.fov, settings.camera.aspect, settings.camera.near, settings.camera.far);

  renderer = new THREE.CSS3DRenderer();

  renderer.setSize(settings.view.width, settings.view.height);

  renderer.domElement.setAttribute("id", "renderer");

  document.body.appendChild(renderer.domElement);

  balloonImage = document.createElement("img");

  balloonImage.setAttribute("id", "player1");

  balloonImage.setAttribute("src", "https://s3-us-west-2.amazonaws.com/s.cdpn.io/160215/balloon.png");

  playerBalloon1 = new THREE.CSS3DObject(balloonImage);

  playerBalloon1.position.z = -800;

  playerBalloon1.position.x = -500;

  playerBalloon1.position.y = 0;

  scene.add(playerBalloon1);

  light = new THREE.DirectionalLight(0xffffff, 1);

  light.position.set(1, 1, 1).normalize();

  scene.add(light);

  movementTime = 2000;

  moveBalloon1 = function() {
    if (Math.round(playerBalloon1.position.y === gameData.player[0].y)) {
      return;
    }
    playerBalloon1.position.y = gameData.player[0].y;
    return playerBalloon1.position.x = gameData.player[0].x;
  };

  render = function() {
    requestAnimationFrame(render);
    moveBalloon1();
    stats.update();
    camera.lookAt(playerBalloon1.position);
    return renderer.render(scene, camera);
  };

  onWindowResize = function(event) {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    return camera.updateProjectionMatrix();
  };

  window.addEventListener('resize', onWindowResize, false);

  onMouseMove = function(event) {
    var inverse, maxHeight, maxWidth, xPosition, xRatio, yPosition, yRatio;
    xRatio = event.clientX / settings.view.width;
    yRatio = event.clientY / settings.view.height;
    maxHeight = 1000;
    maxWidth = 1000;
    inverse = function(n) {
      return n * -1;
    };
    xPosition = (xRatio * maxWidth) - (maxWidth / 2);
    yPosition = (inverse(yRatio) * maxHeight) + (maxHeight / 2);
    gameData.player[0].y = yPosition;
    gameData.player[0].x = xPosition;
    return debug.innerHTML = "x:" + xPosition + " y: " + yPosition;
  };

  window.addEventListener('mousemove', onMouseMove, false);

  render();

  splash.addEventListener("click", function(event) {
    return splash.classList.add("hidden");
  });

  setTimeout(function() {
    return splash.classList.add("hidden");
  }, 5000);

}).call(this);
