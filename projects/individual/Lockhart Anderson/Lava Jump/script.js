var camera, clock, element, example, light, render, renderer, scene, settings;

settings = {
  view: {
    width: window.innerWidth,
    height: window.innerHeight
  },
  camera: {
    fov: 75,
    aspect: window.innerWidth / window.innerHeight,
    near: 0.5,
    far: 1000
  }
};

scene = new THREE.Scene();

clock = new THREE.Clock;

camera = new THREE.PerspectiveCamera(settings.camera.fov, settings.camera.aspect, settings.camera.near, settings.camera.far);

camera.position.z = -400;

renderer = new THREE.CSS3DRenderer({
  antialias: true
});

renderer.setSize(settings.view.width, settings.view.height);

renderer.domElement.setAttribute("id", "renderer");

document.body.appendChild(renderer.domElement);

element = document.createElement("img");

element.setAttribute("id", "standing");

element.setAttribute("src", "https://sites.google.com/a/student.austintrinity.org/code-ing/_/rsrc/1450126747412/pics/New%20Piskel%20%281%29.gif");

example = new THREE.CSS3DObject(element);

example.position.z = -300;

example.position.x = -300;

scene.add(example);

element = document.createElement("img");

element.setAttribute("id", "running2");

element.setAttribute("src", "https://sites.google.com/a/student.austintrinity.org/code-ing/pics/Running%20back%20foot%20foreward%20(2).gif");

example = new THREE.CSS3DObject(element);

example.position.z = -300;

example.position.x = -200;

scene.add(example);

element = document.createElement("img");

element.setAttribute("id", "running1");

element.setAttribute("src", "https://sites.google.com/a/student.austintrinity.org/code-ing/pics/New%20Piskel%20clone.gif");

example = new THREE.CSS3DObject(element);

example.position.z = -300;

example.position.x = 100;

scene.add(example);

element = document.createElement("img");

element.setAttribute("id", "sliding");

element.setAttribute("src", "https://sites.google.com/a/student.austintrinity.org/code-ing/pics/New%20Piskel%20(2).gif");

example = new THREE.CSS3DObject(element);

example.position.z = -300;

example.position.x = -100;

scene.add(example);

element = document.createElement("img");

element.setAttribute("id", "jumping");

element.setAttribute("src", "https://sites.google.com/a/student.austintrinity.org/code-ing/pics/New%20Piskel.gif");

example = new THREE.CSS3DObject(element);

example.position.z = -300;

example.position.x = 0;

scene.add(example);

light = new THREE.DirectionalLight(0xffffff, 1);

light.position.set(1, 1, 1).normalize();

scene.add(light);

render = function() {
  var delta;
  requestAnimationFrame(render);
  example.rotation.x += 0.00;
  example.rotation.z += 0.00;
  delta = clock.getDelta();
  return renderer.render(scene, camera);
};

render();

console.debug("test");