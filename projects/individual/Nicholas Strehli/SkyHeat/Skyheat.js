(function() {
  var balloonImage, camera, gameData, light, moveBalloon1, movementTime, onWindowResize, playerBalloon1, render, renderer, scene, settings, stats, tracker, videoElement;

  settings = {
    view: {
      width: window.innerWidth,
      height: window.innerHeight
    },
    camera: {
      fov: 75,
      aspect: window.innerWidth / window.innerHeight,
      near: 0.1,
      far: 1000
    }
  };

  gameData = {
    player: {
      0: {
        x: 0,
        y: 0,
        z: 0
      },
      1: {
        x: 0,
        y: 0,
        z: 0
      }
    }
  };

  videoElement = document.createElement("video");

  videoElement.setAttribute("id", "trackVideo");

  videoElement.setAttribute("width", settings.view.width);

  videoElement.setAttribute("height", settings.view.height);

  videoElement.setAttribute("preload", true);

  videoElement.setAttribute("autoplay", true);

  videoElement.setAttribute("loop", true);

  videoElement.setAttribute("muted", true);

  document.body.appendChild(videoElement);

  tracker = new tracking.ColorTracker('magenta');

  tracking.track(videoElement, tracker, {
    camera: true
  });

  tracker.on("track", function(event) {
    var playerControls;
    if (!event.data.length) {
      return;
    }
    playerControls = event.data;
    return _.each(playerControls, function(playerControl, i) {
      var xRatio, yPosition, yRatio;
      yRatio = playerControl.y / settings.view.height;
      xRatio = playerControl.x / settings.view.width;
      yPosition = (yRatio * settings.view.height) * -1 + settings.view.height + -(settings.view.height / 3);
      return gameData.player[i].y = yPosition;
    });
  });

  scene = new THREE.Scene();

  stats = new Stats;

  document.body.appendChild(stats.domElement);

  camera = new THREE.PerspectiveCamera(settings.camera.fov, settings.camera.aspect, settings.camera.near, settings.camera.far);

  renderer = new THREE.CSS3DRenderer();

  renderer.setSize(settings.view.width, settings.view.height);

  renderer.domElement.setAttribute("id", "renderer");

  document.body.appendChild(renderer.domElement);

  balloonImage = document.createElement("img");

  balloonImage.setAttribute("id", "player1");

  balloonImage.setAttribute("src", "https://s3-us-west-2.amazonaws.com/s.cdpn.io/160215/balloon.png");

  playerBalloon1 = new THREE.CSS3DObject(balloonImage);

  playerBalloon1.position.z = -800;

  scene.add(playerBalloon1);

  light = new THREE.DirectionalLight(0xffffff, 1);

  light.position.set(1, 1, 1).normalize();

  scene.add(light);

  movementTime = 2000;

  moveBalloon1 = _.throttle(function() {
    var tween;
    if (Math.round(playerBalloon1.position.y === gameData.player[0].y)) {
      return;
    }
    tween = new TWEEN.Tween(playerBalloon1.position).to({
      y: gameData.player[0].y
    }, movementTime).easing(TWEEN.Easing.Quadratic.Out);
    return tween.start();
  }, 1000);

  render = function() {
    requestAnimationFrame(render);
    moveBalloon1();
    stats.update();
    TWEEN.update();
    return renderer.render(scene, camera);
  };

  onWindowResize = function(event) {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    return camera.updateProjectionMatrix();
  };

  window.addEventListener('resize', onWindowResize, false);

  render();

}).call(this);
