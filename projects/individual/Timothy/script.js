// Create a terminal callback function
var terminalElement = $('#commandInput');
var terminalOutput  = $("#commandOutputMain > #reply");
var terminalArtwork = $("#commandOutputMain > #artwork");

// Set terminal options
var terminalOptions = {
	greetings: '',
	name	 : 'gameName',
	height	 : 100,
	prompt	 : '&#10093;'
}

/* TEMPLATE
	"": function(userInput, terminal){
		
	},
	*/

// Context
var context       = {}
var context_Start = {}
var context_Name  = {}
var context_Inventory = {}

// Terminal commands
var terminalCommands = {
	"1": function(userInput, terminal){
		context_Start.hasRunOne = true
		terminal.echo("Type start to begin");
		var output = "Welcome to the TEGE! This is a demo made for the TEGE, or Terminal Emulator Game Engine. This is a demo, and is very short, but showcases the uses of the TEGE. Please enjoy. (Type 'start' to begin.)"
		terminalOutput.html(output);
	},
	
	"start": function(userInput, terminal){
		if(!context_Start.hasRunOne){ return}
		//Initializes player's inventory and area
		context_Inventory.hasTorch = true;
		context_Inventory.numberCoins = 15;
		context_Inventory.hasCoins = true;
		context_Inventory.hasKnife = true;
		context_Inventory.knifeLevel = 1;
		context_Inventory.hasPotions = true;
		context_Inventory.numberPotions = 2;
		context.area = 1;
		//Displays info in upper window
		var output = "Before you lies a large, dark cavern. In your inventory, you have a torch, a leather pouch containing 15 gold coins, a weak knife, and two health potions. Directly infront of you is a small crevice in the cavern wall. You cannot see any light coming out of it. To your left, a passageway. Nailed to the wall next to the passage way is a wooden slab covered in scratches, with a very weak knife stabbed into it.";
		//Displays prompt in console
		terminal.echo("To go through the crevice in the wall, type 'crevice'. To go through the passageway, type 'passageway'.");
		terminalOutput.html(output);
	},
	
	"crevice": function(userInput, terminal){
		if(!context_Start.hasRunOne){ return}
		context.Crevice = true	
		var output = "You walk up to the crevice in the wall, and begin to climb through. You just barely make it through. You pull yourself into a new cavern, which is pitch black, save from the light seeping in from the crevice you came through. Unfortunetly, there is not enough light to see anything.";
		terminal.echo("To use your torch, type 'torch'. To return to the area you came from, type 'return'.");
		terminalOutput.html(output);
	},
	
		"passageway": function(userInput, terminal){
			if(!context_Start.hasRunOne){ return}
			if(!context.Crevice){return}
      context.Passageway1 = true;
			var output = "You enter the passage way. The claustraphopic walls are lined in a greyish, rock like material, yet they feel strangly hollow. As you countinue down the hall, you see an outline of a figure aproaching you slowly."
			terminal.echo("Type 'countinue' to countinue the text")
			terminalOutput.html(output);
	},
	
	"engine": function(userInput, terminal){
	//window.onLoad = document.getElementById("creditsOne").style.visibility = "hidden";
  //document.getElementById("creditsOne").style.visibility = "visible";
		terminal.clear()
	}
};

// Create the terminal callback function
terminalCallback = function(userInput, terminal){
	// Get the function to run for the command
	command = terminalCommands[userInput]
	if(command){ command(userInput, terminal)	}
	
}

// Create the user terminal
terminalElement.terminal(terminalCallback, terminalOptions)
// document.getElementById("commandOutputMain").innerHTML

window.onload = gameStart();

/*
function outputTest() {
	var inputMain = document.getElementById("commandInput").value;document.getElementById("commandOutputMain").innerHTML = inputMain;
}
*/
/*function startTyped() {
	function inputSend() {
	var latestInput = document.getElementById("commandInput").value;
	//console.log(localStorage.latestInput);	
  
		if(latestInput == 1) {
		console.log("Good job!");
		}
		
		if(latestInput == 2) {
			console.log("2 was typed");
		}
		}
}
*/
function gameStart() {
	 output = "Type '1' in the text field to begin"
	 terminalOutput.html(output);
	 terminalOutput.hide();
	 setTimeout(function(){
	  terminalArtwork.fadeOut(function(){
	    terminalOutput.fadeIn();
	  });
	 }, 2000);
}

/*
function inputSend() {
	window.latestInput = document.getElementById("commandInput").value;
	//console.log(localStorage.latestInput);	
}
*/
//window.onload = startTyped()

/*
function outputSend() {
	var inputMain = document.getElementById("commandInput").value;
  localStorage.latestInput = inputMain;
	if(localStorage.latestInput = "gg") {
		console.log(localStorage.latestInput);
	};

}
*/
