# Code Lab Settings
module.exports =
  
  # Tasks (Grunt)
  tasks:
    rootFolder  : "#{process.cwd()}"
    inputFolder : '/public_source'
    outputFolder: "/public"
    watchFiles  : [
      'public_source/**/*.jade'
      'public_source/**/*.coffee'
      'public_source/**/*.styl'
    ]
      