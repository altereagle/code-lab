
// Require these modules
var async   = require("async");      // async
var _       = require("underscore"); // underscore
var colors  = require("colors");     // colors
var express = require("express");    // express
var socket  = require("socket.io");  // socket.io

var app  = express();
app.use("/", express.static(__dirname + "/game ideas/socket app/"))
var http = require("http").Server(app)
var io   = require("socket.io")(http);

var port = process.env.PORT;

http.listen(port, function(){
  console.log("Listening for traffic on port", port);
});

io.on("connection", function(socket){
  socket.on("greeting channel", function(data){
    var message = data.message;
    console.log(message);

    io.emit("response")
  });

});

