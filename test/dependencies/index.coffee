# Dependencies
# ========
# Dependencies for running experiments of most kinds can be found here.

# Test Dependencies
chai   = require 'chai'
should = chai.should()
expect = chai.expect

# Path to the dependencies file to test
filePath     = "#{process.cwd()}/dependencies"

# Globals
dependencies = undefined


# Start the test
# ==============
# Dependencies
# ============
# Node modules used throughout the application are
# checked for their isntallation here.
# - **TODO: The depenencies here should also be in the dependencies section of
# the package.json for this application.(Add tests for this)**
chai   = require 'chai'
should = chai.should()
expect = chai.expect

fs           = require "fs"
path         = "#{process.cwd()}/dependencies"
dependencies = undefined

describe "Dependencies", ->
  
  it "should exist", (done)->
    @timeout 5000
    dependencies = require path
    dependencies.should.exist
    done()

# Utilities
# ---------
# These are types of modules which do server tasks
# such as writing, reading and removing local files from the server.
# They are among most often used dependencies throughout the server.
describe "Dependencies: Utilities", ->
  it "should have .path", (done)->
    dependencies.path.should.exist
    done()

  it "should have .fs", (done)->
    dependencies.fs.should.exist
    done()

  it "should have .process for child process spawning", (done)->
    dependencies.process.should.exist
    done()

  it "should have .walk for file system path walking", (done)->
    dependencies.walk.should.exist
    done()

  it "should have .mkdirp to make directories", (done)->
    dependencies.mkdirp.should.exist
    done()

  it "should have .watch", (done)->
    dependencies.watch.should.exist
    done()

  it "should have .colors for awesomeness", (done)->
    dependencies.colors.should.exist
    done()

  it "should have .async", (done)->
    dependencies.async.should.exist
    done()

  it "should have .promise (bluebird)", (done)->
    dependencies.promise.should.exist
    done()

  it "should have .underscore", (done)->
    dependencies._.should.exist
    done()

  it "should have .watch", (done)->
    dependencies.watch.should.exist
    done()

  it "should have .mustache for templating", (done)->
    dependencies.mustache.should.exist
    done()

# Security
# --------
# These are a types of modules used to secure person sensitive information.
# They are also responsible for monitoring server traffic
# for early hazard detection.
describe "Dependencies: Security", ->
  it "should have .logger for express logging", (done)->
    dependencies.logger.should.exist
    done()
    
  it "should have .winston for logging", (done)->
    dependencies.winston.should.exist
    done()
    
# Database
# --------
# These are the database connection methods the server uses for data
# sharing between clients.
describe "Dependencies: Database", ->
  it "should have .database.neo4j", (done)->
    dependencies.database.should.exist
    dependencies.database.neo4j.should.exist
    done()

# Web Services
# ------------
# These are the modules which communicate, translate and manage web protocols.
describe "Dependencies: Web Services", ->
  it "should have .express", (done)->
    dependencies.express.should.exist
    done()

  it "should have .http", (done)->
    dependencies.http.should.exist
    done()

  it "should have .requestify", (done)->
    dependencies.requestify.should.exist
    done()

  it "should have .session (express)", (done)->
    dependencies.session.should.exist
    done()

  it "should have .morgan (logging)", (done)->
    dependencies.morgan.should.exist
    done()

  it "should have .cookieParser (express)", (done)->
    dependencies.cookieParser.should.exist
    done()

  it "should have .bodyParser (express)", (done)->
    dependencies.bodyParser.should.exist
    done()

  it "should have .errorHandler (express)", (done)->
    dependencies.errorHandler.should.exist
    done()

  it "should have .logger (express)", (done)->
    dependencies.logger.should.exist
    done()

  it "should have .jade", (done)->
    dependencies.jade.should.exist
    done()

  it "should have .jquery", (done)->
    dependencies.jquery.should.exist
    done()

  it "should have .stylus", (done)->
    dependencies.stylus.should.exist
    done()

  it "should have .renderer (stylus)", (done)->
    dependencies.renderer.should.exist
    done()

  it "should have .nib (stylus)", (done)->
    dependencies.nib.should.exist
    done()

  it "should have .coffeeScript", (done)->
    dependencies.coffeeScript.should.exist
    done()

  it "should have .docco", (done)->
    dependencies.docco.should.exist
    done()

  it "should have .cheerio", (done)->
    dependencies.cheerio.should.exist
    done()

# Web Sockets
# -----------
# These are for speedy client server transatctions.
describe "Dependencies: Web Sockets", ->
  it "should have .io for Socket Io", (done)->
    dependencies.io.should.exist
    done()

  it "should have .bone", (done)->
    dependencies.bone.should.exist
    done()