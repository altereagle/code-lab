# Settings
# ========
# Settings for running experiments of most kinds can be found here.

# Test Dependencies
chai   = require 'chai'
should = chai.should()
expect = chai.expect

# Dependencies
dependencies = require "#{process.cwd()}/dependencies"

# Path to the settings file to test
filePath     = "#{process.cwd()}/settings"

# Globals
settings     = undefined


# Start the test
# ==============
describe "Settings", ->
  
  it "should exist", (done)->
    settings = require filePath
    settings.should.exist
    done()