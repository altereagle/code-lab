# Setup testing dependencies
# --------------------------
chai   = require 'chai'
should = chai.should()
expect = chai.expect

# Run test modules
# ----------------
describe "Code Lab -", ->
  require "./dependencies"
  require "./settings"